# Update Development Package Managers
# Composer
# NPM
alias devupdate='composer global update; composer global clearcache; npm update -g; npm cache verify'
alias devupgrade='composer global self-update; npm install -g npm'
alias devup='devupgrade; devupdate'
