# Clean .DS_Store files in current directory
alias dsprune='sudo find . -name ".DS_Store" -depth -exec rm {} \;'
