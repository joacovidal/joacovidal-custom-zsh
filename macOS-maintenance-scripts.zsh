# Run all mac maintenance scripts at once or select specific
alias osxclean='osxcleandaily; osxcleanweekly; osxcleanmonthly'
alias osxcleandaily='sudo periodic daily; echo "macOS daily script finished"'
alias osxcleanweekly='sudo periodic weekly; echo "macOS weekly script finished"'
alias osxcleanmonthly='sudo periodic monthly; echo "macOS monthly script finished"'

alias macclean=osxclean
alias maccleand=osxcleandaily
alias maccleanw=osxcleanweekly
alias maccleanm=osxcleanmonthly
