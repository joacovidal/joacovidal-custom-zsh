# Homebrew's sbin was not found in your PATH but you have installed
# formulae that put executables in /usr/local/sbin.
# Consider setting the PATH for example like so
export PATH="/usr/local/sbin:$PATH"


# Brew unattended update and upgrade
alias brewup='brew update; brew upgrade; brew cleanup; brew doctor'

# Cask unatended upgrade
alias caskup='brew cu -y -a; brew cleanup; brew doctor'

# Full System upgrade (also updates Oh My ZSH)
alias systemup='softwareupdate --all --install --force; brewup; omz update; caskup'
