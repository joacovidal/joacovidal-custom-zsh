# if [[ -z $ZSH_THEME_JOACO_PREFIX ]]; then
#     ZSH_THEME_JOACO_PREFIX=''
# fi
#
# PROMPT='%{$fg_bold[cyan]%}$ZSH_THEME_JOACO_PREFIX %{$fg_bold[green]%}%p %{$fg[green]%}%c %{$fg_bold[cyan]%}$(git_prompt_info)%{$fg_bold[blue]%} % %{$reset_color%}'

PROMPT='%{$fg_bold[green]%}%p%{$fg[green]%}%c%{$fg_bold[cyan]%}$(git_prompt_info)%{$fg_bold[blue]%}%{$reset_color%} '

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[green]%} [%{$fg[cyan]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[green]%}] %{$fg_bold[yellow]%}✳%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[green]%}]"
